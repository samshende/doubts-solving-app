import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    TouchableOpacity,
    ScrollView,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage,
    Picker,
    BackHandler,
    Systrace
} from 'react-native';

import { launchCamera} from 'react-native-image-picker';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';


export default class CreateDoubts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            proFileName: '',
            photoUri: '',
            inProcess: false,
            error: false,
            disabled: false,
            subjectName: '',
            Doubt: '',
            DoubtsListArray:[]
        };
    }

    requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "Cool Photo App Camera Permission",
                    message:
                        "Cool Photo App needs access to your camera " +
                        "so you can take awesome pictures.",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // console.log("grand", granted)
                // console.log("You can use the camera");
            } else {
                // console.log("grand", granted)
                // console.log("Camera permission denied");
            }
        } catch (err) {
            console.log(err);
        }
    };

    componentDidMount = () => {
        this.requestCameraPermission();

        AsyncStorage.getItem('DoubtsList')
        .then(data => {
            let data1 = JSON.parse(data);
            // console.log(data1)
            if (data1 != null) {
                this.setState({
                    DoubtsListArray:data1
                })
            }
        })
        .catch(err => console.log('Error in login page', err));

    }






    selectPhotoTappedcam = async () => {
        // console.log("inside image")
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
            },
        };
        // console.log("Img", options)
        launchCamera(options, response => {
            // console.log('Response showImagePicker = ', response);
            if (response.didCancel) {
                //   console.log('User cancelled photo picker');
            } else if (response.error) {
                //   console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //   console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = { uri: response.uri, fileName: response.fileName };
                // console.log("source",response)
                // return
                // console.log("source",`data:image/png;base64,${response.data}`)
                const image = "data:image/jpeg;base64," + response.data
                // console.log("ss.....",image)
                this.setState({
                    proFileName: response.fileName,
                    photoUri: response.uri,
                    error: false,
                });
                
            }
        });
    }

    delete = () => {
        this.setState({
            proFileName: '',
            photoUri: '',
        });
    };

    handleSubmit = async () => {
        let myArray=this.state.DoubtsListArray
        if (this.state.subjectName == '' || this.state.Doubt == '' || this.state.photoUri == '') {
            this.setState({
                error: true
            })
            return false
        }

        else {
            this.setState({
                disabled: true
            })

            let obj={
                Subject:this.state.subjectName,
                Doubts:this.state.Doubt,
                ImageName:this.state.proFileName,
                PhotoURL:this.state.photoUri
            }
            console.log(obj)
            myArray.push(obj)
            console.log(myArray)

            try {
                await AsyncStorage.setItem('DoubtsList', JSON.stringify(myArray));

                Toast.show({
                    text: 'Doubt saved successfully...!!',
                    type: 'success',
                    position: 'center',
                    duration: 2000,
                    style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 50 },
                    textStyle: {
                      textAlign: 'center',
                      color: 'white'
                    }
                  });

                  this.props.navigation.navigate('DoubtsList',{
                      name:this.state.subjectName
                  })
              } catch (error) {
                Toast.show({
                    text: 'Somthing WentWrong...!!',
                    type: 'danger',
                    position: 'center',
                    duration: 2000,
                    style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 50 },
                    textStyle: {
                      textAlign: 'center',
                      color: 'black'
                    }
                  });
                // Error saving data
              }

        }

    }

    



    render() {

        return (
            <Container>
                <View style={{ backgroundColor: '#00BCE1', height: 50, width: wp('100%') }}>
                    <Text style={{ textAlign: 'center', marginTop: 10, fontWeight: 'bold', color: 'white', fontSize: 20 }}>Create Doubts</Text>
                </View>
                <ScrollView>

                    <Text style={{ marginTop: 20, marginLeft: 20 }}>Subjects:</Text>
                    <Textarea style={{ backgroundColor: 'white', width: wp('90%'), alignSelf: 'center', borderRadius: 10, marginTop: 10 }}
                        placeholderTextColor='grey' rowSpan={1.8} bordered placeholder="Please enter Subject"
                        onChangeText={data =>
                            this.setState({ subjectName: data, error: false })
                        }
                    />
                    {this.state.error && this.state.subjectName == '' &&
                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                            Please enter Subject Name...{' '}
                        </Text>
                    }

                    <Text style={{ marginTop: 10, marginLeft: 20 }}>Your Doubt:</Text>

                    <Textarea style={{ backgroundColor: 'white', width: wp('90%'), alignSelf: 'center', borderRadius: 10, marginTop: 10 }}
                        placeholderTextColor='grey' rowSpan={5} bordered placeholder="Please enter Doubt"
                        onChangeText={data =>
                            this.setState({ Doubt: data, error: false })
                        }
                    />

                    {this.state.error && this.state.Doubt == '' &&
                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                            Please enter Doubt...{' '}
                        </Text>
                    }

                    {this.state.photoUri != '' && (
                        <Card rounded style={styles.editStyle}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                <Image
                                    source={{ uri: this.state.photoUri }}
                                    style={{ width: 70, height: 80 }}
                                />
                                <View
                                    style={{
                                        position: 'absolute',
                                        right: 3,
                                        top: 2,
                                        height: 20,
                                        width: 20,
                                        backgroundColor: '#ccc',
                                        alignItems: 'center',
                                        borderRadius: 10,
                                        justifyContent: 'center',
                                    }}>
                                    <TouchableWithoutFeedback onPress={() => this.delete()}>
                                        <Text style={{ fontSize: 14 }}>X</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </Card>
                    )}
                    {this.state.photoUri == '' && (
                        <>
                            <Text style={{ marginTop: 10, marginLeft: 20 }}>Uploade Image:</Text>

                            <Card style={styles.editStyle}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
                                    {/* <View style={{ justifyContent: 'space-between', width: wp('60%'), alignSelf: 'center', alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}> */}

                                    <TouchableWithoutFeedback
                                        onPress={this.selectPhotoTappedcam}>
                                        <Text style={styles.fabIcon} >+</Text>
                                    </TouchableWithoutFeedback>
                                    {/* </View> */}
                                </View>
                            </Card>
                        </>
                    )}

                    {this.state.error && this.state.photoUri == '' &&
                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                            please Uploade Image.....{' '}
                        </Text>
                    }

                    <Button disabled={this.state.disabled}
                        onPress={() => { this.handleSubmit() }}
                        full style={[styles.button, { backgroundColor: this.state.disabled ? "#5cc2d6" : "#00BCE1" }]}>
                        <Text>save</Text></Button>
                </ScrollView>
            </Container>
        )
    }

}



const styles = StyleSheet.create({
    viewHeader: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: wp('90%'),
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    viewHeader1: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    icon: {
        alignSelf: 'center'
    },
    text: {
        marginLeft: 20,
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    button: {
        width: wp('90%'),
        borderRadius: 7,
        alignSelf: 'center',
        // backgroundColor: '#00BCE1',
        marginTop: 20,
        marginBottom: 20
        // position:'absolute',
        // bottom:0,
    },
    editStyle: {
        backgroundColor: '#fff',
        height: 130,
        width: wp('90%'),
        borderRadius: 10,
        alignSelf: 'center',
        marginTop: 10
    },
    fabIcon: {
        alignSelf: 'center',
        fontSize: 40,
        color: 'grey'
    },

});