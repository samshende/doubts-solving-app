import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    AsyncStorage,
    Dimensions
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Text, View, Card,
    Item, Input,Icon
} from 'native-base';
import PhotoView from 'react-native-photo-view-ex';

const { width } = Dimensions.get('window');
const height = width * 0.8;
export default class DoubtsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disabled: false,
            name: "sam",
            DoubtsListArray: [],
            DoubtsListArray1: []

        };
    }


    componentDidMount = async() => {
        // console.log("CDM ::");
        this.Fetch()
    }

    Fetch = async () => {
        AsyncStorage.getItem('DoubtsList')
            .then(data => {
                let data1 = JSON.parse(data);
                // console.log(data1)
                if (data1 != null) {
                    this.setState({
                        DoubtsListArray: data1,
                        DoubtsListArray1:data1
                    })
                }
            })
            .catch(err => console.log('Error in login page', err));


    }

    componentWillReceiveProps = () => {
        // console.log("CWR ::");
        this.Fetch()

    }

    chnageInput = (data) => {
        // console.log("dddd", data)
        if (data == '') {
            this.setState({
                DoubtsListArray1: this.state.DoubtsListArray
            })
        }

        let arr = []
        this.state.DoubtsListArray.map((i, j) => {
            if (i.Subject.toLowerCase().includes(data.toLowerCase())) {
                arr.push(i)
            }
        })
        this.setState({
            DoubtsListArray1: arr
        })

        if (data == '') {
            this.setState({
                DoubtsListArray1: this.state.DoubtsListArray
            })
        }
    }


    render() {
        return (
            <Container>
               
                <View style={{ backgroundColor: '#00BCE1', height: 50, width: wp('100%') }}>
                    <Text 
                    style={{ textAlign: 'center', 
                    marginTop: 10, fontWeight: 'bold', 
                    color: 'white', fontSize: 20 }}>Doubts List</Text>
                </View>
                <Header searchBar rounded style={{ backgroundColor: '#00BCE1', elevation: 0 }}>
                    <Item>
                        <Icon onPress={this.searchName} name="ios-search" />
                        <Input placeholder="Search by Subject Name "
                            onChangeText={this.chnageInput.bind(this)} />
                        {/* <Filter onPress={() => { this.setState({ modalVisible: true }) }} style={{ marginRight: 10 }} /> */}
                    </Item>
                </Header>

                <View>
                <View style={{ marginTop: 10,marginLeft:10 }}>
                            <Text style={{ marginLeft: 10, marginBottom: 5 }}>Showing {this.state.DoubtsListArray1.length} results</Text>
                        </View>
                    <Text 
                    onPress={() => { this.props.navigation.navigate("CreateDoubts") }} 
                    style={{ position: 'absolute', 
                    right: 10, top:-15, color:'#00BCE1',
                    fontWeight: 'bold', fontSize: 50 }}>
                        +
                    </Text>
                </View>

                <ScrollView style={{marginTop:20,marginBottom:20}}>

                    {this.state.DoubtsListArray1.length==0
                    &&
                    <Text style={{ alignSelf: 'center', flex: 1, justifyContent: 'center', marginTop: 100 }}>No record is available.</Text>
                    }
                    {this.state.DoubtsListArray1.length>0 && this.state.DoubtsListArray1.map((name,id)=>{
                        return(
                            <Card style={{
                                backgroundColor: '#fff',
                                height:"auto",
                                width: wp('90%'),
                                borderRadius: 10,
                                alignSelf: 'center',
                                marginTop: 10
                            }}>
  <View style={{ alignContent: 'center', alignItems: 'center', flex: 1 }} >
                            <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',color:'grey',marginTop:20,marginBottom:5}}>{name.Subject}</Text>
                                {/* <ImageZoom cropWidth={width}
                                    cropHeight={350}
                                    imageWidth={width}
                                    imageHeight={350}>
                                    <Image
                                        style={
                                            styles.image
                                        }
                                        resizeMode='center'
                                        source={{ uri: name.PhotoURL }}

                                    />
                                </ImageZoom> */}

                                <PhotoView

                                    zoomTransitionDuration={1000}
                                    source={{ uri: name.PhotoURL }}
                                    minimumZoomScale={1}
                                    maximumZoomScale={3}
                                    resizeMode="contain"
                                    onLoad={() => console.log("Image loaded!")}
                                    style={styles.image}
                                />
                                     <Text style={{ position: 'absolute', 
                                     bottom: 190, 
                                     zIndex: 200, fontWeight: 'bold',
                                      backgroundColor: '#616061', 
                                      color: 'white' }}>Double Tap to zoom</Text>

                            <Text style={{textAlign:'center',fontSize:16,fontWeight:'bold',color:'grey',marginTop:10,marginBottom:20}}>{name.Doubts}</Text>

                            </View>
                            </Card>
                        )
                    })}
                </ScrollView>
            </Container>

        )
    }
}


const styles = StyleSheet.create({
    viewHeader: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: wp('90%'),
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    viewHeader1: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    icon: {
        alignSelf: 'center'
    },
    text: {
        marginLeft: 20,
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    button: {
        width: wp('90%'),
        borderRadius: 7,
        alignSelf: 'center',
        // backgroundColor: '#00BCE1',
        marginTop: 20,
        marginBottom: 20
        // position:'absolute',
        // bottom:0,
    },
    editStyle: {
        backgroundColor: '#fff',
        height: 130,
        width: wp('90%'),
        borderRadius: 10,
        alignSelf: 'center',
        marginTop: 10
    },
    fabIcon: {
        alignSelf: 'center',
    },
    image: {
        width: wp('85%'),
        height: 300,
        maxHeight: 300,
        maxWidth: wp('85%'),
        // resizeMode: 'stretch'
    },

});