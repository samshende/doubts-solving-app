
import React from 'react';
import {createStackNavigator} from 'react-navigation'
import DoubtsList from './Component/DoubtsList'
import CreateDoubts from './Component/CreateDoubts'
import StartPage from './Component/StartPage'
import {
    Root
  } from 'native-base';

const Navigator=createStackNavigator(

    {
        DoubtsList:{
            screen:DoubtsList,navigationOptions:{
                header:null
            },
        },
        CreateDoubts:{
            screen:CreateDoubts,navigationOptions:{
                header:null
            },
        },
        StartPage:{
            screen:StartPage,navigationOptions:{
                header:null
            },
        },
    },

    {
        initialRouteName:'StartPage',
        headerMode:'none',
        mode:'Model'
    }
)


const App=(props)=>(
    <Root>
        <Navigator />
    </Root>
)

export default App;